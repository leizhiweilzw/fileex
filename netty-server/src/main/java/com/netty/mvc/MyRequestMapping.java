package com.netty.mvc;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.METHOD})
public @interface MyRequestMapping {

    String value();

    MyRequestMethod method() default MyRequestMethod.GET;
}
